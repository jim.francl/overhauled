﻿# The script of the game goes in this file.
#image splash = "splash.png"

define t = Character('test block dialog')

label splashscreen:
    scene black
    with Pause(2)

    show text "303 Studio with Capt. JollyRoger Presents..." with dissolve
    with Pause(2)

    hide text with dissolve
    with Pause(2)

    scene skyline
    with Pause(2)

    show text "In a future..." at right 
    with moveinright 
    with Pause(2)

    hide text with dissolve
    with Pause(1)

    show text "A solution is needed..." at right
    with moveinright
    with Pause(2)

    hide text with dissolve
    with Pause(1)

    return

default current_location = "apartment_main"

label start:
   
    "You are at home." with fade
    show text "You are at home." with dissolve
    with Pause(1)
    hide text with dissolve
    
    jump apartment_main
