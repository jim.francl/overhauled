screen hotspottwo():
    imagebutton:
        xalign 0.0
        yalign 0.5
        auto "images/left_exit_%s.png"
        action Jump("apartment_hall")
    
    imagebutton:
        xalign 1.0
        yalign 0.5
        auto "images/right_exit_%s.png"
        action Jump("apartment_bed")


screen hotspotbed(): 
    imagebutton:
        xalign 0.0
        yalign 0.5
        auto "images/left_exit_%s.png"
        action Jump("apartment_main")
        
screen hotspotright():
    imagebutton:
        xalign 1.0
        yalign 0.5
        auto "images/right_exit_%s.png"

screen hotspothall():
    imagebutton:
        xalign 0.0
        yalign 0.5
        auto "images/left_exit_%s.png"
        action Jump("apartment_main")


screen inventory():
        add "inventory-ui.png" xalign 0.5 yalign 1.0
