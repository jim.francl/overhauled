# Overhauled

![title.svg](images/title.svg "Overhauled"){width=75%}

## Introduction

A tribute to the 1990s point-and-click that I grew up playing. A science fiction adventure games about gathering items and manipulating parts of the enviroments to progress the game along with the story driven goals. Developed with Ren'py, but not following conventional Visual Novel styles and story telling.  Pushing the limits of Ren'py to give the player a more interactive game.  

## Target Audience
Nastalgia orientaed and without complicated ideas. With intuitive-to-grasp mechanics, this game is marketed to at least casual game players who are up for puzzling challenges.

## Genre
Singleplayer, Science Fiction, Adventure, Point-and-Click, Visual Novel.

## Platform
The game is developed to be released on Windows and Linux PC

## Development Software
    • Ren’Py for programming
    • Aseprite, Gimp, Inkscape for graphics and UI
    • Ardour, and Audacity for Soundtrack and SFX

